# Online Store Project 🛒

This is a backend-only implementation of an online store using ASP.NET Web API and EntityFramework, PostgreSQL database, and Docker for deployment.

## Task 
Implement an online store with the following functionality:

#### Homepage
- Display product categories.
- Navigation breadcrumbs.
#### Products Page
- Display subcategories.
- List products in selected categories.
#### Product Page
- Display product image, title, and description.
- Quantity selector.
#### Checkout Page
- Display shopping cart.

## Technologies
- Backend: ASP.NET Web MVC without frontend.
- Database: PostgreSQL.
- Deployment: Docker.
- Nugets: EntityFramework, Automapper, Swagger, JWToken...

## Structure of project
Clean application architecture was selected.<br>
Repository pattern was used to implement queries in the database

- Domain: including the entities, value objects, and domain services;
- Application: contains implements the application services, DTOs (data transfer objects), and mappers;
- Infrastructure: contains the implementation of data access, logging, email, and other communication mechanisms;
- Presentation: the main project contains the presentation layer and implements the ASP.NET Core web API.

## Setup Instructions

### 1. Prerequisites

- [.NET Core SDK](https://dotnet.microsoft.com/download) installed
- [Docker Desktop](https://www.docker.com/products/docker-desktop) installed

### 2. Clone the Repository

```bash
git clone https://gitlab.com/zhGorbachov/onlinestore.git
cd repo
```

### 3. Setup PostgreSQL Database
Run PostgreSQL in a Docker container:

```bash
docker run --name postgres-db -e POSTGRES_PASSWORD=your_password -d -p 5432:5432 postgres
```

Create a new database and user:

```bash
docker exec -it postgres-db psql -U postgres
CREATE DATABASE online_store;
CREATE USER store_user WITH ENCRYPTED PASSWORD 'your_password';
GRANT ALL PRIVILEGES ON DATABASE online_store TO store_user;
```

### 4. Install Dependencies

```bash
dotnet restore
```

### 5. Update Database

```bash
dotnet ef database update
```

### 6. Run the Application

```bash
dotnet run
```

## API Endpoints examples
- <b>POST</b> `/CreateToken` - Create authorize token for user by data;
- <b>PUT</b> `/Update user` - Update user object on another in database by id;
- <b>DELETE</b> `/Delete review` - Delete review from database by id; 
- <b>GET</b> `/Get image by id` - Get image of product from database if it contains by id;
- <b>GET</b> `/LoginUser` - Get authorize token if user exists and the data fits;
- <b>Get</b> `/Get all addresses` - Get all addresses from database.

## Testing
Use tools like Postman or endpoint of application `/swagger/index.html` to test API endpoints.