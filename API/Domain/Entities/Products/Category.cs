﻿using Domain.Common;

namespace Domain.Entities.Products;

public class Category : BaseEntity
{
    public string Name { get; set; }
    public string? Description { get; set; }
    public Guid? ParentCategoryId { get; set; }
    
    public virtual Category? ParentCategory { get; set; }
    public virtual ICollection<Category>? SubCategories { get; set; }
    public virtual ICollection<Product>? Products { get; set; }
}