﻿using Domain.Common;
using Domain.Entities.Users;

namespace Domain.Entities.Products;

public class Rating : BaseEntity
{
    public int Grade { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
    
    public virtual User User { get; set; }
    public virtual Product Product { get; set; }
}