﻿using Domain.Common;
using Domain.Entities.Users;

namespace Domain.Entities.Products;

public class Review : BaseEntity
{
    public string Text { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
    public DateTime ReviewDate = DateTime.UtcNow;
    
    public virtual User User { get; set; }
    public virtual Product Product { get; set; }
}