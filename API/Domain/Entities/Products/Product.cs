﻿using Domain.Common;
using Domain.Entities.Orders;

namespace Domain.Entities.Products;

public class Product : BaseEntity
{
    public string Title { get; set; }
    public string? Description { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public byte[]? Image { get; set; }
    public Guid CategoryId { get; set; }
    
    public virtual Category Category { get; set; }
    public virtual ICollection<Rating>? Ratings { get; set; }
    public virtual ICollection<Review>? Reviews { get; set; }
    public virtual ICollection<OrderItem>? OrderItems { get; set; }
}