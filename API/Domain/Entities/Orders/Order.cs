﻿using Domain.Common;
using Domain.Entities.Users;

namespace Domain.Entities.Orders;

public class Order : BaseEntity
{
    public decimal TotalAmount { get; set; }
    public bool Status { get; set; }
    public DateTime OrderDate = DateTime.UtcNow;
    
    public string OrderId { get; set; }
    public Guid UserId { get; set; }
    public Guid PaymentMethodId { get; set; }
    public string ShippingAddress { get; set; }
    public Guid BillingAddressId { get; set; }
    
    public virtual Address Address { get; set; }
    public virtual PaymentMethod PaymentMethod { get; set; }
    public virtual ICollection<OrderItem> OrderItems { get; set; }
    public virtual User User { get; set; }
}