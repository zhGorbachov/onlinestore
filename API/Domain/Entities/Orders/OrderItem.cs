﻿using Domain.Common;
using Domain.Entities.Products;

namespace Domain.Entities.Orders;

public class OrderItem : BaseEntity
{
    public int Quantity { get; set; }
    public decimal Subtotal { get; set; }
    public Guid OrderId { get; set; }
    public Guid ProductId { get; set; }
    
    public virtual Product Product { get; set; }
    public virtual Order Order { get; set; }
}