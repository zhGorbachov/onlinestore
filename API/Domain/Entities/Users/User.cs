﻿using Domain.Common;
using Domain.Entities.Orders;
using Domain.Entities.Products;

namespace Domain.Entities.Users;

public class User : BaseEntity
{
    public string FirstName { get; set; }
    public string SecondName { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    
    public virtual ICollection<Order>? Orders { get; set; }
    public virtual ICollection<Review>? Reviews { get; set; }
    public virtual ICollection<Rating>? Ratings { get; set; }
    public virtual ICollection<PaymentMethod>? PaymentMethods { get; set; }
    public virtual Address? Address { get; set; }
}