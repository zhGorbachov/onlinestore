﻿using Domain.Common;
using Domain.Entities.Orders;

namespace Domain.Entities.Users;

public class PaymentMethod : BaseEntity
{
    public string CardNumber { get; set; }
    public string ExpireDate { get; set; }
    public Guid UserId { get; set; }
    
    public virtual User User { get; set; }
    public virtual ICollection<Order>? Orders { get; set; }
}