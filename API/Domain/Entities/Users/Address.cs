﻿using Domain.Common;
using Domain.Entities.Orders;

namespace Domain.Entities.Users;

public class Address : BaseEntity
{
    public string StreetAddress { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }
    public Guid UserId { get; set; }
    
    public virtual User User { get; set; }
    public virtual ICollection<Order>? Orders { get; set; }
}