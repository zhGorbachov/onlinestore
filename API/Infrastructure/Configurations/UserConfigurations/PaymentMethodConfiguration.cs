﻿using Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configurations.UserConfigurations;

public class PaymentMethodConfiguration : IEntityTypeConfiguration<PaymentMethod>
{
    public void Configure(EntityTypeBuilder<PaymentMethod> builder)
    {
        builder.HasIndex(x => x.Id);
        builder.Property(x => x.CardNumber).IsRequired().IsUnicode().HasMaxLength(19);
        builder.Property(x => x.ExpireDate).IsRequired();
    }
}