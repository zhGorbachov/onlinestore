﻿using Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configurations.UserConfigurations;

public class AddressConfiguration : IEntityTypeConfiguration<Address>
{
    public void Configure(EntityTypeBuilder<Address> builder)
    {
        builder.HasIndex(x => x.Id);
        builder.Property(x => x.City).IsRequired();
        builder.Property(x => x.StreetAddress).IsRequired();
        builder.Property(x => x.ZipCode).IsRequired();
    }
}