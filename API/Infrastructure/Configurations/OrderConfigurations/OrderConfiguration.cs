﻿using Domain.Entities.Orders;
using Domain.Entities.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Configurations.OrderConfigurations;

public class OrderConfiguration : IEntityTypeConfiguration<Order>
{
    public void Configure(EntityTypeBuilder<Order> builder)
    {
        builder.HasIndex(x => x.Id);
        builder.Property(x => x.OrderId).IsRequired().HasMaxLength(16);
        builder.Property(x => x.TotalAmount).IsRequired();
        builder.Property(x => x.Status).IsRequired();
        builder.Property(x => x.OrderDate).IsRequired();
        builder.Property(x => x.ShippingAddress).IsRequired();
        
        builder
            .HasMany(x => x.OrderItems)
            .WithOne(x => x.Order)
            .HasForeignKey(x => x.OrderId)
            .HasPrincipalKey(x => x.Id)
            .OnDelete(DeleteBehavior.SetNull);
        
        builder
            .HasOne(x => x.Address)
            .WithMany(x => x.Orders)
            .HasForeignKey(x => x.BillingAddressId)
            .HasPrincipalKey(x => x.Id)
            .OnDelete(DeleteBehavior.SetNull);
        
        builder
            .HasOne(x => x.PaymentMethod)
            .WithMany(x => x.Orders)
            .HasForeignKey(x => x.PaymentMethodId)
            .HasPrincipalKey(x => x.Id)
            .OnDelete(DeleteBehavior.SetNull);
    }
}