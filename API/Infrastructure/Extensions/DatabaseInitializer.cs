﻿using Infrastructure.Context;

namespace Infrastructure.Extensions;

public class DatabaseInitializer
{
    public static void Initialize(DataContext context)
    {
        context.Database.EnsureCreated();
    }
}