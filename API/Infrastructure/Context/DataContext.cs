﻿using Domain.Entities.Orders;
using Domain.Entities.Products;
using Domain.Entities.Users;
using Infrastructure.Configurations.OrderConfigurations;
using Infrastructure.Configurations.ProductConfigurations;
using Infrastructure.Configurations.UserConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Context;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
        
    }
    
    public DbSet<User> Users { get; set; }
    public DbSet<Address> Addresses { get; set; }
    public DbSet<PaymentMethod> PaymentMethods { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderItem> OrderItems { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Review> Reviews { get; set; }
    public DbSet<Rating> Ratings { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new UserConfiguration());
        modelBuilder.ApplyConfiguration(new AddressConfiguration());
        modelBuilder.ApplyConfiguration(new PaymentMethodConfiguration());
        modelBuilder.ApplyConfiguration(new CategoryConfiguration());
        modelBuilder.ApplyConfiguration(new ProductConfiguration());
        modelBuilder.ApplyConfiguration(new RatingConfiguration());
        modelBuilder.ApplyConfiguration(new ReviewConfiguration());
        modelBuilder.ApplyConfiguration(new OrderConfiguration());
        modelBuilder.ApplyConfiguration(new OrderItemConfiguration());

        base.OnModelCreating(modelBuilder);
        DataSeeding(modelBuilder);
    }

    private void DataSeeding(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>()
            .HasData(
                new User
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Dima",
                    SecondName = "Shylo",
                    Username = "Gor0bchyk",
                    Password = "qwerty123",
                    Email = "Gorobunchik_Dimasik_Pivasik@domain.com"
                });

        modelBuilder.Entity<User>()
            .HasData(
                new User
                {
                    Id = Guid.Parse("c1cf40fb-151b-4136-a268-0778dccc7e12"),
                    FirstName = "Daniil",
                    SecondName = "Diordica",
                    Username = "PRD0412",
                    Password = "password123",
                    Email = "prd0412@gmail.com"
                });

        modelBuilder.Entity<PaymentMethod>()
            .HasData(
                new PaymentMethod
                {
                    Id = Guid.NewGuid(),
                    CardNumber = "4004400440044004",
                    ExpireDate = "12/12/12",
                    UserId = Guid.Parse("c1cf40fb-151b-4136-a268-0778dccc7e12")
                });

        modelBuilder.Entity<Address>()
            .HasData(
                new Address
                {
                    Id = Guid.NewGuid(),
                    City = "Kropivnitskiy",
                    StreetAddress = "Popova 26",
                    ZipCode = "25022",
                    UserId = Guid.Parse("c1cf40fb-151b-4136-a268-0778dccc7e12")
                });

        modelBuilder.Entity<Category>()
            .HasData(
                new Category
                {
                    Id = Guid.Parse("57a63828-b55e-4011-8c94-d49846c11799"),
                    Name = "Car`s",
                    Description = "All connected with cars.",
                });

        modelBuilder.Entity<Category>()
            .HasData(
                new Category
                {
                    Id = Guid.NewGuid(),
                    Name = "Engines",
                    Description = "All connected with car`s engines.",
                    ParentCategoryId = Guid.Parse("57a63828-b55e-4011-8c94-d49846c11799")
                });
        
        modelBuilder.Entity<Category>()
            .HasData(
                new Category
                {
                    Id = Guid.Parse("792a853e-12ff-4f0f-90d3-449c22e0fc5e"),
                    Name = "Wheels",
                    Description = "All connected with car`s wheels.",
                    ParentCategoryId = Guid.Parse("57a63828-b55e-4011-8c94-d49846c11799")
                });
        
        modelBuilder.Entity<Product>()
            .HasData(
                new Product
                {
                    Id = Guid.Parse("0715e0a2-33f3-4178-9602-b79835ab5477"),
                    Title = "BMW wheels M4",
                    Description = "Wheels for BMW M4.",
                    Quantity = 16,
                    Price = 299,
                    CategoryId = Guid.Parse("792a853e-12ff-4f0f-90d3-449c22e0fc5e")
                });
        
        modelBuilder.Entity<Product>()
            .HasData(
                new Product
                {
                    Id = Guid.NewGuid(),
                    Title = "Kia picanto",
                    Quantity = 1,
                    Price = 120300,
                    CategoryId = Guid.Parse("57a63828-b55e-4011-8c94-d49846c11799")
                });

        modelBuilder.Entity<Rating>()
            .HasData(
                new Rating
                {
                    Id = Guid.NewGuid(),
                    Grade = 4,
                    ProductId = Guid.Parse("0715e0a2-33f3-4178-9602-b79835ab5477"),
                    UserId = Guid.Parse("c1cf40fb-151b-4136-a268-0778dccc7e12")
                });
        
        modelBuilder.Entity<Review>()
            .HasData(
                new Review
                {
                    Id = Guid.NewGuid(),
                    Text = "Cool, but to expensive",
                    ProductId = Guid.Parse("0715e0a2-33f3-4178-9602-b79835ab5477"),
                    UserId = Guid.Parse("c1cf40fb-151b-4136-a268-0778dccc7e12")
                });
    }
}