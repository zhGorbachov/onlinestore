﻿using Application.Repositories.ProductRepositories;
using Domain.Entities.Products;
using Infrastructure.Context;

namespace Infrastructure.Repositories.ProductRepositories;

public class ProductRepository : BaseRepository<Product>, IProductRepository
{
    public ProductRepository(DataContext context) : base(context)
    {
    }

    public IQueryable<Product?> GetProductsByCategory(Guid id)
    {
        var products = GetAll().Where(x => x.CategoryId == id);
        return products;
    }
    
    public async Task<int?> GetQuantityOfProductAsync(Guid id)
    {
        var product = await GetByIdAsync(id);
        return product?.Quantity;
    }

    public async Task<byte[]?> GetImageProductByIdAsync(Guid id)
    {
        var product = await GetByIdAsync(id);
        return product?.Image;
    }

    public async Task AddImageToProductByIdAsync(Guid id, byte[] image)
    {
        var product = await GetByIdAsync(id);
        product.Image = image;
        await UpdateAsync(product, id);
    }
}