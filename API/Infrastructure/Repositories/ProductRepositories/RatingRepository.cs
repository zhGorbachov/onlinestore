﻿using Application.Repositories.ProductRepositories;
using Domain.Entities.Products;
using Infrastructure.Context;

namespace Infrastructure.Repositories.ProductRepositories;

public class RatingRepository : BaseRepository<Rating>, IRatingRepository
{
    public RatingRepository(DataContext context) : base(context)
    {
    }
}