﻿using Application.Repositories.ProductRepositories;
using Domain.Entities.Products;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.ProductRepositories;

public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
{
    public CategoryRepository(DataContext context) : base(context)
    {
    }

    public async Task<IQueryable<Category>?> GetSubCategoriesAsync(Guid id)
    {
        var categories = GetAll().Where(x => x.ParentCategoryId == id);

        return categories;
    }
}