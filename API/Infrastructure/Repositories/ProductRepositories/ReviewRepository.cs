﻿using Application.Repositories.ProductRepositories;
using Domain.Entities.Products;
using Infrastructure.Context;

namespace Infrastructure.Repositories.ProductRepositories;

public class ReviewRepository : BaseRepository<Review>, IReviewRepository
{
    public ReviewRepository(DataContext context) : base(context)
    {
    }
}