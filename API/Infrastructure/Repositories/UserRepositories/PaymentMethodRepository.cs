﻿using Application.Repositories;
using Application.Repositories.UserRepositories;
using Domain.Entities.Users;
using Infrastructure.Context;

namespace Infrastructure.Repositories.UserRepositories;

public class PaymentMethodRepository : BaseRepository<PaymentMethod>, IPaymentMethodRepository
{
    public PaymentMethodRepository(DataContext context) : base(context)
    {
    }
}