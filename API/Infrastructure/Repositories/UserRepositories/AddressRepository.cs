﻿using Application.Repositories.UserRepositories;
using Domain.Entities.Users;
using Infrastructure.Context;

namespace Infrastructure.Repositories.UserRepositories;

public class AddressRepository : BaseRepository<Address>, IAddressRepository
{
    public AddressRepository(DataContext context) : base(context)
    {
    }
}