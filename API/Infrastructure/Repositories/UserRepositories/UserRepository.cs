﻿using Application.Repositories;
using Application.Repositories.UserRepositories;
using Domain.Entities.Users;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.UserRepositories;

public class UserRepository : BaseRepository<User>, IUserRepository 
{
    public UserRepository(DataContext context) : base(context)
    {
    }

    public async Task<User> GetByUsernameAsync(string username)
    {
        var entity = await GetAll().FirstOrDefaultAsync(x => x.Username == username);
        return entity;
    }

    public async Task<User> GetByEmailAsync(string email)
    {
        var entity = await GetAll().FirstOrDefaultAsync(x => x.Email == email);
        return entity;
    }
}