﻿using Application.Repositories.OrderRepositories;
using Domain.Entities.Orders;
using Infrastructure.Context;

namespace Infrastructure.Repositories.OrderRepositories;

public class OrderRepository : BaseRepository<Order>, IOrderRepository
{
    public OrderRepository(DataContext context) : base(context)
    {
    }

    public async Task AddOrderItemToOrderAsync(OrderItem orderItem, Guid id)
    {
        var order = await GetByIdAsync(id);
        order?.OrderItems.Add(orderItem);
    }

    public async Task<ICollection<OrderItem>?> GetAllOrderItemsFromOrderAsync(Guid id)
    {
        var order = await GetByIdAsync(id);
        return order?.OrderItems;
    }
}