﻿using Application.Repositories.OrderRepositories;
using Domain.Entities.Orders;
using Infrastructure.Context;

namespace Infrastructure.Repositories.OrderRepositories;

public class OrderItemRepository : BaseRepository<OrderItem>, IOrderItemRepository
{
    public OrderItemRepository(DataContext context) : base(context)
    {
    }
}