﻿using Application.Models.OrderModels;
using Application.Models.ProductModels;
using Application.Models.UserModels;
using AutoMapper;
using WebAPI.Models.DTOs.OrderDTOs;
using WebAPI.Models.DTOs.OrderResponses;
using WebAPI.Models.DTOs.ProductDTOs;
using WebAPI.Models.DTOs.ProductResponses;
using WebAPI.Models.DTOs.UserDTOs;
using WebAPI.Models.DTOs.UserResponses;

namespace WebAPI.Mapper;

public class WebMapperProfile : Profile
{
    public WebMapperProfile()
    {
        CreateMap<UserModel, UserResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ForMember(x => x.Username,
                opt => opt.MapFrom(dto => dto.Username))
            .ForMember(x => x.Password,
                opt => opt.MapFrom(dto => dto.Password))
            .ForMember(x => x.Email,
                opt => opt.MapFrom(dto => dto.Email))
            .ReverseMap();

        CreateMap<PaymentMethodModel, PaymentMethodResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<AddressModel, AddressResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<OrderModel, OrderResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<OrderItemModel, OrderItemResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<CategoryModel, CategoryResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<ProductModel, ProductResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<RatingModel, RatingResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<ReviewModel, ReviewResponse>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<UserModel, UserDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ForMember(x => x.Username,
                opt => opt.MapFrom(dto => dto.Username))
            .ForMember(x => x.Password,
                opt => opt.MapFrom(dto => dto.Password))
            .ForMember(x => x.Email,
                opt => opt.MapFrom(dto => dto.Email))
            .ReverseMap();
        
        CreateMap<AddressModel, AddressDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<PaymentMethodModel, PaymentMethodDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<OrderModel, OrderDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<OrderItemModel, OrderItemDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<ProductModel, ProductDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<CategoryModel, CategoryDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<RatingModel, RatingDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
        
        CreateMap<ReviewModel, ReviewDTO>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(dto => dto.Id))
            .ReverseMap();
    }
}