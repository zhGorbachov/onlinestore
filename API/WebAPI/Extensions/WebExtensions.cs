﻿using System.Reflection;

namespace WebAPI.Extensions;

public static class WebExtensions
{
    public static void ConfigureWeb(this IServiceCollection services)
    {
        services.AddAutoMapper(Assembly.GetExecutingAssembly());
    }
}