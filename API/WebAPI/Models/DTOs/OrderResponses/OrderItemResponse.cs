﻿namespace WebAPI.Models.DTOs.OrderResponses;

public class OrderItemResponse : BaseResponse
{
    public int Quantity { get; set; }
    public decimal Subtotal { get; set; }
    public Guid OrderId { get; set; }
    public Guid ProductId { get; set; }
}