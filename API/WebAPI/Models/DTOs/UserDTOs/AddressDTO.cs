﻿namespace WebAPI.Models.DTOs.UserDTOs;

public class AddressDTO : BaseDTO
{
    public string StreetAddress { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }
    public Guid UserId { get; set; }
}