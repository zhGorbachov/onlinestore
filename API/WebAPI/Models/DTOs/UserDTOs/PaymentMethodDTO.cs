﻿namespace WebAPI.Models.DTOs.UserDTOs;

public class PaymentMethodDTO : BaseDTO
{
    public string CardNumber { get; set; }
    public string ExpireDate { get; set; }
    public Guid UserId { get; set; }
}