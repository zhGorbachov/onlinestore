﻿namespace WebAPI.Models.DTOs.UserDTOs;

public class UserDTO : BaseDTO
{
    public string FirstName { get; set; }
    public string SecondName { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
}