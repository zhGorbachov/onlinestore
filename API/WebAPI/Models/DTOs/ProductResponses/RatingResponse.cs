﻿namespace WebAPI.Models.DTOs.ProductResponses;

public class RatingResponse : BaseResponse
{
    public int Grade { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
}