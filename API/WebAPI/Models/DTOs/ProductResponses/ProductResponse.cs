﻿namespace WebAPI.Models.DTOs.ProductResponses;

public class ProductResponse : BaseResponse
{
    public string Title { get; set; }
    public string? Description { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public byte[]? Image { get; set; }
    public Guid CategoryId { get; set; }
}