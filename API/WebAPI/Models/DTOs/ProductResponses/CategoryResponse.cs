﻿namespace WebAPI.Models.DTOs.ProductResponses;

public class CategoryResponse : BaseResponse
{
    public string Name { get; set; }
    public string? Description { get; set; }
    public Guid? ParentCategoryId { get; set; }
}