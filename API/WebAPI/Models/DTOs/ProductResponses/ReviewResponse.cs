﻿namespace WebAPI.Models.DTOs.ProductResponses;

public class ReviewResponse : BaseResponse
{
    public string Text { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
}