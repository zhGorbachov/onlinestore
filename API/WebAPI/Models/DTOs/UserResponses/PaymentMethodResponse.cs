﻿namespace WebAPI.Models.DTOs.UserResponses;

public class PaymentMethodResponse : BaseResponse
{
    public string CardNumber { get; set; }
    public string ExpireDate { get; set; }
    public Guid UserId { get; set; }
}