﻿namespace WebAPI.Models.DTOs.UserResponses;

public class AddressResponse : BaseResponse
{
    public string StreetAddress { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }
    public Guid UserId { get; set; }
}