﻿namespace WebAPI.Models.DTOs.UserResponses;

public class UserResponse : BaseResponse
{
    public required string FirstName { get; set; }
    public required string SecondName { get; set; }
    public required string Username { get; set; }
    public required string Password { get; set; }
    public required string Email { get; set; }
}