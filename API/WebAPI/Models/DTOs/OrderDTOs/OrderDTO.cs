﻿namespace WebAPI.Models.DTOs.OrderDTOs;

public class OrderDTO : BaseDTO
{
    public decimal TotalAmount { get; set; }
    public bool Status { get; set; }
    public string OrderId { get; set; }
    public Guid UserId { get; set; }
    public Guid PaymentMethodId { get; set; }
    public string ShippingAddress { get; set; }
    public Guid BillingAddressId { get; set; }
}