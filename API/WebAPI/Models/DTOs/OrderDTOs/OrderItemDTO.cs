﻿namespace WebAPI.Models.DTOs.OrderDTOs;

public class OrderItemDTO : BaseDTO
{
    public int Quantity { get; set; }
    public decimal Subtotal { get; set; }
    public Guid OrderId { get; set; }
    public Guid ProductId { get; set; }
}