﻿namespace WebAPI.Models.DTOs.ProductDTOs;

public class ProductDTO : BaseDTO
{
    public string Title { get; set; }
    public string? Description { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public Guid CategoryId { get; set; }
}