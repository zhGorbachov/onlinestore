﻿namespace WebAPI.Models.DTOs.ProductDTOs;

public class CategoryDTO : BaseDTO
{
    public string Name { get; set; }
    public string? Description { get; set; }
    public Guid? ParentCategoryId { get; set; }
}