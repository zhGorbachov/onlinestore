﻿namespace WebAPI.Models.DTOs.ProductDTOs;

public class ReviewDTO : BaseDTO
{
    public string Text { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
}