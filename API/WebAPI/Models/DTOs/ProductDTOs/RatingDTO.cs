﻿namespace WebAPI.Models.DTOs.ProductDTOs;

public class RatingDTO : BaseDTO
{
    public int Grade { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
}