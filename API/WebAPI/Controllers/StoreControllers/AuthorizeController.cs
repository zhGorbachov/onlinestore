﻿using Application.Features.Interfaces;
using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Models.DTOs.UserDTOs;

namespace WebAPI.Controllers.StoreControllers;

public class AuthorizeController : Controller
{
    private readonly ILogger<AuthorizeController> _logger;
    private readonly IMapper _mapper;
    private readonly IAuthorizeService _authorizeService;
    private readonly IUserService _userService;

    public AuthorizeController(ILogger<AuthorizeController> logger, IMapper mapper, IAuthorizeService authorizeService, IUserService userService)
    {
        _logger = logger;
        _mapper = mapper;
        _authorizeService = authorizeService;
        _userService = userService;
    }

    [HttpPost("RegisterUser")]
    public async Task<IActionResult> RegisterUserAsync(UserDTO userDto)
    {
        var userModel = _mapper.Map<UserModel>(userDto);
        await _authorizeService.RegisterUserAsync(userModel);
        
        return Ok();
    }

    [HttpGet("LoginUser")]
    public async Task<IActionResult> LoginUserAsync(string username, string password)
    {
        var user = await _userService.GetUserModeByUsernameAsync(username);
        
        if (user.Password == password)
        {
            var token = _authorizeService.LoginUser(user);
            return Ok(JsonConvert.SerializeObject(token));
        }
        
        return StatusCode(402);
    }
}