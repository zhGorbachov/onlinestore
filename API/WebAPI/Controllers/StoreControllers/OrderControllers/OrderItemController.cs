﻿using Application.Features.Interfaces.OrderServiceInterfaces;
using Application.Models.OrderModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.OrderDTOs;
using WebAPI.Models.DTOs.OrderResponses;

namespace WebAPI.Controllers.StoreControllers.OrderControllers;

public class OrderItemController : Controller
{
    private readonly ILogger<OrderItemController> _logger;
    private readonly IOrderItemService _orderItemService;
    private readonly IMapper _mapper;

    public OrderItemController(ILogger<OrderItemController> logger, IOrderItemService orderItemService, IMapper mapper)
    {
        _logger = logger;
        _orderItemService = orderItemService;
        _mapper = mapper;
    }

    [HttpPost("Add orderItem")]
    public async Task<IActionResult> AddOrderItemAsync(OrderItemDTO orderItemDto)
    {
        var orderItemModel = _mapper.Map<OrderItemModel>(orderItemDto);
        await _orderItemService.AddOrderItemAsync(orderItemModel);

        return Ok("OrderItem Added");
    }

    [HttpPut("Update orderItem")]
    public async Task<IActionResult> UpdateOrderItemAsync(OrderItemDTO orderItemDto, Guid id)
    {
        var orderItemModel = _mapper.Map<OrderItemModel>(orderItemDto);
        await _orderItemService.UpdateOrderItemAsync(orderItemModel, id);

        return Ok("OrderItem updated");
    }

    [HttpDelete("Delete orderItem")]
    public async Task<IActionResult> DeleteOrderItemAsync(Guid id)
    {
        await _orderItemService.DeleteOrderItemAsync(id);

        return Ok("OrderItem deleted");
    }

    [HttpGet("Get orderItem by id")]
    public async Task<IActionResult> GetOrderItemByIdAsync(Guid id)
    {
        var orderItem = await _orderItemService.GetOrderItemByIdAsync(id);
        var orderItemResponse = _mapper.Map<OrderItemResponse>(orderItem);

        return Ok(orderItemResponse);
    }

    [HttpGet("Get all orderItems")]
    public IActionResult GetAllOrderItemAsync()
    {
        var orderItems = _orderItemService.GetAllOrderItemModels();
        var orderItemResponses = _mapper.ProjectTo<OrderItemResponse>(orderItems);

        return Ok(orderItemResponses);
    }
}