﻿using Application.Features.Interfaces.OrderServiceInterfaces;
using Application.Models.OrderModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.OrderDTOs;
using WebAPI.Models.DTOs.OrderResponses;

namespace WebAPI.Controllers.StoreControllers.OrderControllers;

public class OrderController : Controller
{
    private readonly ILogger<OrderController> _logger;
    private readonly IOrderService _orderService;
    private readonly IMapper _mapper;

    public OrderController(ILogger<OrderController> logger, IOrderService orderService, IMapper mapper)
    {
        _logger = logger;
        _orderService = orderService;
        _mapper = mapper;
    }

    [HttpPost("Add order")]
    public async Task<IActionResult> AddOrderAsync(OrderDTO orderDto)
    {
        var orderModel = _mapper.Map<OrderModel>(orderDto);
        await _orderService.AddOrderAsync(orderModel);

        return Ok("Order Added");
    }

    [HttpPut("Update order")]
    public async Task<IActionResult> UpdateOrderAsync(OrderDTO orderDto, Guid id)
    {
        var orderModel = _mapper.Map<OrderModel>(orderDto);
        await _orderService.UpdateOrderAsync(orderModel, id);

        return Ok("Order updated");
    }

    [HttpDelete("Delete order")]
    public async Task<IActionResult> DeleteOrderAsync(Guid id)
    {
        await _orderService.DeleteOrderAsync(id);

        return Ok("Order deleted");
    }

    [HttpGet("Get order by id")]
    public async Task<IActionResult> GetOrderByIdAsync(Guid id)
    {
        var order = await _orderService.GetOrderByIdAsync(id);
        var orderResponse = _mapper.Map<OrderResponse>(order);

        return Ok(orderResponse);
    }

    [HttpGet("Get all orders")]
    public IActionResult GetAllOrderAsync()
    {
        var orders = _orderService.GetAllOrderModels();
        var orderResponses = _mapper.ProjectTo<OrderResponse>(orders);

        return Ok(orderResponses);
    }

    [HttpPost("Add order item to order")]
    public async Task<IActionResult> AddOrderItemToOrderAsync(OrderItemDTO orderItemDto, Guid id)
    {
        var orderItem = _mapper.Map<OrderItemModel>(orderItemDto);
        await _orderService.AddOrderItemModelToOrderAsync(orderItem, id);
        return Ok("Order item added to order");
    }

    [HttpPost("Get all order items from order")]
    public async Task<IActionResult> GetAllOrderItemsFromOrderAsync(Guid id)
    {
        var orderItems = await _orderService.GetAllOrderItemModelsFromOrderAsync(id);
        return Ok(orderItems);
    }
}