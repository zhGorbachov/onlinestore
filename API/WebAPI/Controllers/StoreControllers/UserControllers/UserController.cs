﻿using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.UserDTOs;
using WebAPI.Models.DTOs.UserResponses;

namespace WebAPI.Controllers.StoreControllers.UserControllers;

public class UserController : Controller
{
    private readonly ILogger<UserController> _logger;
    private readonly IUserService _userService;
    private readonly IMapper _mapper;

    public UserController(ILogger<UserController> logger, IUserService userService, IMapper mapper)
    {
        _logger = logger;
        _userService = userService;
        _mapper = mapper;
    }

    [HttpPost("Add user")]
    public async Task<IActionResult> AddUserAsync(UserDTO userDto)
    {
        var userModel = _mapper.Map<UserModel>(userDto);
        await _userService.AddUserAsync(userModel);
        
        return Ok("User Added");
    }

    [HttpPut("Update user")]
    public async Task<IActionResult> UpdateUserAsync(UserDTO userDto, Guid id)
    {
        var userModel = _mapper.Map<UserModel>(userDto);
        await _userService.UpdateUserAsync(userModel, id);
        
        return Ok("User updated");
    }

    [HttpDelete("Delete user")]
    public async Task<IActionResult> DeleteUserAsync(Guid id)
    {
        await _userService.DeleteUserAsync(id);

        return Ok("User deleted");
    }

    [HttpGet("Get user by id")]
    public async Task<IActionResult> GetUserByIdAsync(Guid id)
    {
        var user = await _userService.GetUserByIdAsync(id);
        var userResponse = _mapper.Map<UserResponse>(user);

        return Ok(userResponse);
    }

    [HttpGet("Get all users")]
    public IActionResult GetAllUserAsync()
    {
        var users = _userService.GetAllUserModels();
        var userResponses = _mapper.ProjectTo<UserResponse>(users);

        return Ok(userResponses);
    }
    
    [HttpGet("Get user by username")]
    public async Task<IActionResult> GetUserByUsernameAsync(string username)
    {
        var user = await _userService.GetUserModeByUsernameAsync(username);
        var userResponse = _mapper.Map<UserResponse>(user);

        return Ok(userResponse);
    }
    
    [HttpGet("Get user by email")]
    public async Task<IActionResult> GetUserByEmailAsync(string email)
    {
        var user = await _userService.GetUserModelByEmailAsync(email);
        var userResponse = _mapper.Map<UserResponse>(user);

        return Ok(userResponse);
    }
}