﻿using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.UserDTOs;
using WebAPI.Models.DTOs.UserResponses;

namespace WebAPI.Controllers.StoreControllers.UserControllers;

public class AddressController : Controller
{
    private readonly ILogger<AddressController> _logger;
    private readonly IAddressService _addressService;
    private readonly IMapper _mapper;

    public AddressController(ILogger<AddressController> logger, IAddressService addressService, IMapper mapper)
    {
        _logger = logger;
        _addressService = addressService;
        _mapper = mapper;
    }

    [HttpPost("Add address")]
    public async Task<IActionResult> AddAddressAsync(AddressDTO addressDto)
    {
        var addressModel = _mapper.Map<AddressModel>(addressDto);
        await _addressService.AddAddressAsync(addressModel);

        return Ok("Address Added");
    }

    [HttpPut("Update address")]
    public async Task<IActionResult> UpdateAddressAsync(AddressDTO addressDto, Guid id)
    {
        var addressModel = _mapper.Map<AddressModel>(addressDto);
        await _addressService.UpdateAddressAsync(addressModel, id);

        return Ok("Address updated");
    }

    [HttpDelete("Delete address")]
    public async Task<IActionResult> DeleteAddressAsync(Guid id)
    {
        await _addressService.DeleteAddressAsync(id);

        return Ok("Address deleted");
    }

    [HttpGet("Get address by id")]
    public async Task<IActionResult> GetAddressByIdAsync(Guid id)
    {
        var address = await _addressService.GetAddressByIdAsync(id);
        var addressResponse = _mapper.Map<AddressResponse>(address);

        return Ok(addressResponse);
    }

    [HttpGet("Get all addresses")]
    public IActionResult GetAllAddressAsync()
    {
        var addresses = _addressService.GetAllAddressModels();
        var addressResponses = _mapper.ProjectTo<AddressResponse>(addresses);

        return Ok(addressResponses);
    }
}