﻿using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.UserDTOs;
using WebAPI.Models.DTOs.UserResponses;

namespace WebAPI.Controllers.StoreControllers.UserControllers;

public class PaymentMethodController : Controller
{
    private readonly ILogger<PaymentMethodController> _logger;
    private readonly IPaymentMethodService _paymentMethodService;
    private readonly IMapper _mapper;

    public PaymentMethodController(ILogger<PaymentMethodController> logger, IPaymentMethodService paymentMethodService,
        IMapper mapper)
    {
        _logger = logger;
        _paymentMethodService = paymentMethodService;
        _mapper = mapper;
    }

    [HttpPost("Add paymentMethod")]
    public async Task<IActionResult> AddPaymentMethodAsync(PaymentMethodDTO paymentMethodDto)
    {
        var paymentMethodModel = _mapper.Map<PaymentMethodModel>(paymentMethodDto);
        await _paymentMethodService.AddPaymentMethodAsync(paymentMethodModel);

        return Ok("PaymentMethod Added");
    }

    [HttpPut("Update paymentMethod")]
    public async Task<IActionResult> UpdatePaymentMethodAsync(PaymentMethodDTO paymentMethodDto, Guid id)
    {
        var paymentMethodModel = _mapper.Map<PaymentMethodModel>(paymentMethodDto);
        await _paymentMethodService.UpdatePaymentMethodAsync(paymentMethodModel, id);

        return Ok("PaymentMethod updated");
    }

    [HttpDelete("Delete paymentMethod")]
    public async Task<IActionResult> DeletePaymentMethodAsync(Guid id)
    {
        await _paymentMethodService.DeletePaymentMethodAsync(id);

        return Ok("PaymentMethod deleted");
    }

    [HttpGet("Get paymentMethod by id")]
    public async Task<IActionResult> GetPaymentMethodByIdAsync(Guid id)
    {
        var paymentMethod = await _paymentMethodService.GetPaymentMethodByIdAsync(id);
        var paymentMethodResponse = _mapper.Map<PaymentMethodResponse>(paymentMethod);

        return Ok(paymentMethodResponse);
    }

    [HttpGet("Get all paymentMethods")]
    public IActionResult GetAllPaymentMethodAsync()
    {
        var paymentMethods = _paymentMethodService.GetAllPaymentMethodModels();
        var paymentMethodResponses = _mapper.ProjectTo<PaymentMethodResponse>(paymentMethods);

        return Ok(paymentMethodResponses);
    }
}