﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.ProductDTOs;
using WebAPI.Models.DTOs.ProductResponses;

namespace WebAPI.Controllers.StoreControllers.ProductControllers;

public class RatingController : Controller
{
    private readonly ILogger<RatingController> _logger;
    private readonly IRatingService _ratingService;
    private readonly IMapper _mapper;

    public RatingController(ILogger<RatingController> logger, IRatingService ratingService, IMapper mapper)
    {
        _logger = logger;
        _ratingService = ratingService;
        _mapper = mapper;
    }

    [HttpPost("Add rating")]
    public async Task<IActionResult> AddRatingAsync(RatingDTO ratingDto)
    {
        var ratingModel = _mapper.Map<RatingModel>(ratingDto);
        await _ratingService.AddRatingAsync(ratingModel);

        return Ok("Rating Added");
    }

    [HttpPut("Update rating")]
    public async Task<IActionResult> UpdateRatingAsync(RatingDTO ratingDto, Guid id)
    {
        var ratingModel = _mapper.Map<RatingModel>(ratingDto);
        await _ratingService.UpdateRatingAsync(ratingModel, id);

        return Ok("Rating updated");
    }

    [HttpDelete("Delete rating")]
    public async Task<IActionResult> DeleteRatingAsync(Guid id)
    {
        await _ratingService.DeleteRatingAsync(id);

        return Ok("Rating deleted");
    }

    [HttpGet("Get rating by id")]
    public async Task<IActionResult> GetRatingByIdAsync(Guid id)
    {
        var rating = await _ratingService.GetRatingByIdAsync(id);
        var ratingResponse = _mapper.Map<RatingResponse>(rating);

        return Ok(ratingResponse);
    }

    [HttpGet("Get all ratings")]
    public IActionResult GetAllRatingAsync()
    {
        var ratings = _ratingService.GetAllRatingModels();
        var ratingResponses = _mapper.ProjectTo<RatingResponse>(ratings);

        return Ok(ratingResponses);
    }
}