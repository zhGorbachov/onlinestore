﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.ProductDTOs;
using WebAPI.Models.DTOs.ProductResponses;

namespace WebAPI.Controllers.StoreControllers.ProductControllers;

public class CategoryController : Controller
{
    private readonly ILogger<CategoryController> _logger;
    private readonly ICategoryService _categoryService;
    private readonly IMapper _mapper;

    public CategoryController(ILogger<CategoryController> logger, ICategoryService categoryService, IMapper mapper)
    {
        _logger = logger;
        _categoryService = categoryService;
        _mapper = mapper;
    }

    [HttpPost("Add category")]
    public async Task<IActionResult> AddCategoryAsync(CategoryDTO categoryDto)
    {
        var categoryModel = _mapper.Map<CategoryModel>(categoryDto);
        await _categoryService.AddCategoryAsync(categoryModel);

        return Ok("Category Added");
    }

    [HttpPut("Update category")]
    public async Task<IActionResult> UpdateCategoryAsync(CategoryDTO categoryDto, Guid id)
    {
        var categoryModel = _mapper.Map<CategoryModel>(categoryDto);
        await _categoryService.UpdateCategoryAsync(categoryModel, id);

        return Ok("Category updated");
    }

    [HttpDelete("Delete category")]
    public async Task<IActionResult> DeleteCategoryAsync(Guid id)
    {
        await _categoryService.DeleteCategoryAsync(id);

        return Ok("Category deleted");
    }

    [HttpGet("Get category by id")]
    public async Task<IActionResult> GetCategoryByIdAsync(Guid id)
    {
        var category = await _categoryService.GetCategoryByIdAsync(id);
        var categoryResponse = _mapper.Map<CategoryResponse>(category);

        return Ok(categoryResponse);
    }

    [HttpGet("Get all categories")]
    public IActionResult GetAllCategoryAsync()
    {
        var categories = _categoryService.GetAllCategoryModels();
        var categoryResponses = _mapper.ProjectTo<CategoryResponse>(categories);

        return Ok(categoryResponses);
    }

    [HttpGet("Get subcategories")]
    public async Task<IActionResult> GetSubCategoriesAsync(Guid id)
    {
        var categories = await _categoryService.GetSubCategoryModelsAsync(id);
        var categoryResponses = _mapper.ProjectTo<CategoryResponse>(categories);

        return Ok(categoryResponses);
    }
}