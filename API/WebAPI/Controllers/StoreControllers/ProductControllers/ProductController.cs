﻿using System.Net;
using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.Swagger.Annotations;
using WebAPI.Models.DTOs.ProductDTOs;
using WebAPI.Models.DTOs.ProductResponses;

namespace WebAPI.Controllers.StoreControllers.ProductControllers;

public class ProductController : Controller
{
    private readonly ILogger<ProductController> _logger;
    private readonly IProductService _productService;
    private readonly IMapper _mapper;

    public ProductController(ILogger<ProductController> logger, IProductService productService, IMapper mapper)
    {
        _logger = logger;
        _productService = productService;
        _mapper = mapper;
    }

    [HttpPost("Add product")]
    public async Task<IActionResult> AddProductAsync(ProductDTO productDto, IFormFile image)
    {
        var productModel = _mapper.Map<ProductModel>(productDto);
        
        if (image.Length > 0)
        {
            using var ms = new MemoryStream();
            await image.CopyToAsync(ms);
            var imageBytes = ms.ToArray();
            productModel.Image = imageBytes;
        }
        
        await _productService.AddProductAsync(productModel);

        return Ok("Product Added");
    }

    [HttpPut("Update product")]
    public async Task<IActionResult> UpdateProductAsync(ProductDTO productDto, Guid id)
    {
        var productModel = _mapper.Map<ProductModel>(productDto);
        await _productService.UpdateProductAsync(productModel, id);

        return Ok("Product updated");
    }

    [HttpDelete("Delete product")]
    public async Task<IActionResult> DeleteProductAsync(Guid id)
    {
        await _productService.DeleteProductAsync(id);

        return Ok("Product deleted");
    }

    [HttpGet("Get product by id")]
    public async Task<IActionResult> GetProductByIdAsync(Guid id)
    {
        var product = await _productService.GetProductByIdAsync(id);
        var productResponse = _mapper.Map<ProductResponse>(product);

        return Ok(productResponse);
    }

    [HttpGet("Get all products")]
    public IActionResult GetAllProductAsync()
    {
        var products = _productService.GetAllProductModels();
        var productResponses = _mapper.ProjectTo<ProductResponse>(products);
        
        return Ok(productResponses);
    }

    [HttpGet("Get quantity by id")]
    public async Task<IActionResult> GetQuantityOfProductAsync(Guid id)
    {
        var quantity = await _productService.GetQuantityOfProductAsync(id);
        return Ok(quantity);
    }

    [HttpGet("Get image by id")]
    public async Task<IActionResult> GetImageProductByIdAsync(Guid id)
    {
        var image = await _productService.GetImageProductByIdAsync(id);
        return File(image ?? Array.Empty<byte>(), "image/png");
    }
    
    [HttpGet("Get products by category")]
    public IActionResult GetProductsByCategory(Guid idCategory)
    {
        var products = _productService.GetProductModelsByCategory(idCategory);
        var productResponse = _mapper.ProjectTo<ProductResponse>(products);
        
        return Ok(productResponse);
    }

    [HttpPost("Add image to product")]
    public async Task<IActionResult> AddImageToProductByIdAsync(Guid id, IFormFile image)
    {
        if (image.Length <= 0) return StatusCode(404);
        using var ms = new MemoryStream();
        await image.CopyToAsync(ms);
        var imageBytes = ms.ToArray();
        await _productService.AddImageToProductModelByIdAsync(id, imageBytes);

        return Ok("Image added");
    }
}