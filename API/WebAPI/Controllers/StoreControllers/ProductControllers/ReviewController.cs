﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.ProductDTOs;
using WebAPI.Models.DTOs.ProductResponses;

namespace WebAPI.Controllers.StoreControllers.ProductControllers;

public class ReviewController : Controller
{
    private readonly ILogger<ReviewController> _logger;
    private readonly IReviewService _reviewService;
    private readonly IMapper _mapper;

    public ReviewController(ILogger<ReviewController> logger, IReviewService reviewService, IMapper mapper)
    {
        _logger = logger;
        _reviewService = reviewService;
        _mapper = mapper;
    }

    [HttpPost("Add review")]
    public async Task<IActionResult> AddReviewAsync(ReviewDTO reviewDto)
    {
        var reviewModel = _mapper.Map<ReviewModel>(reviewDto);
        await _reviewService.AddReviewAsync(reviewModel);

        return Ok("Review Added");
    }

    [HttpPut("Update review")]
    public async Task<IActionResult> UpdateReviewAsync(ReviewDTO reviewDto, Guid id)
    {
        var reviewModel = _mapper.Map<ReviewModel>(reviewDto);
        await _reviewService.UpdateReviewAsync(reviewModel, id);

        return Ok("Review updated");
    }

    [HttpDelete("Delete review")]
    public async Task<IActionResult> DeleteReviewAsync(Guid id)
    {
        await _reviewService.DeleteReviewAsync(id);

        return Ok("Review deleted");
    }

    [HttpGet("Get review by id")]
    public async Task<IActionResult> GetReviewByIdAsync(Guid id)
    {
        var review = await _reviewService.GetReviewByIdAsync(id);
        var reviewResponse = _mapper.Map<ReviewResponse>(review);

        return Ok(reviewResponse);
    }

    [HttpGet("Get all reviews")]
    public IActionResult GetAllReviewAsync()
    {
        var reviews = _reviewService.GetAllReviewModels();
        var reviewResponses = _mapper.ProjectTo<ReviewResponse>(reviews);

        return Ok(reviewResponses);
    }
}