﻿using Application.Features.Interfaces;
using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Models.DTOs.UserDTOs;

namespace WebAPI.Controllers.StoreControllers;

public class JwtController : Controller
{
    private readonly ILogger<JwtController> _logger;
    private readonly IMapper _mapper;
    private readonly IJwtService _jwtService;
    private readonly IUserService _userService;

    public JwtController(ILogger<JwtController> logger, IMapper mapper, IJwtService jwtService, IUserService userService)
    {
        _logger = logger;
        _mapper = mapper;
        _jwtService = jwtService;
        _userService = userService;
    }

    [HttpPost("CreateToken")]
    public async Task<IActionResult> CreateJwt(UserDTO userDto)
    {
        var userModel = _mapper.Map<UserModel>(userDto);
        var user = await _userService.GetUserModeByUsernameAsync(userModel.Username);
        return Ok(_jwtService.CreateJwt(user));
    }
}