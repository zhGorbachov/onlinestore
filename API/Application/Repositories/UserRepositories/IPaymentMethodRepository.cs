﻿using Domain.Entities.Users;

namespace Application.Repositories.UserRepositories;

public interface IPaymentMethodRepository : IBaseRepository<PaymentMethod>
{
    
}