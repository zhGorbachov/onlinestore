﻿using Domain.Entities.Users;

namespace Application.Repositories.UserRepositories;

public interface IUserRepository : IBaseRepository<User>
{
    public Task<User> GetByUsernameAsync(string username);
    public Task<User> GetByEmailAsync(string email);
}