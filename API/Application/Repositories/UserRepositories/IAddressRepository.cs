﻿using Domain.Entities.Users;

namespace Application.Repositories.UserRepositories;

public interface IAddressRepository : IBaseRepository<Address>
{
    
}