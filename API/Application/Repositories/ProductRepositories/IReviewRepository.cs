﻿using Domain.Entities.Products;

namespace Application.Repositories.ProductRepositories;

public interface IReviewRepository : IBaseRepository<Review>
{
    
}