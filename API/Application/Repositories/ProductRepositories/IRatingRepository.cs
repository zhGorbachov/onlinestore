﻿using Domain.Entities.Products;

namespace Application.Repositories.ProductRepositories;

public interface IRatingRepository : IBaseRepository<Rating>
{
    
}