﻿using Domain.Entities.Products;

namespace Application.Repositories.ProductRepositories;

public interface ICategoryRepository : IBaseRepository<Category>
{
    public Task<IQueryable<Category>?> GetSubCategoriesAsync(Guid id);
}