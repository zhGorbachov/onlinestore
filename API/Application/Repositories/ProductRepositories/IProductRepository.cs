﻿using Domain.Entities.Products;

namespace Application.Repositories.ProductRepositories;

public interface IProductRepository : IBaseRepository<Product>
{
    public Task<int?> GetQuantityOfProductAsync(Guid id);
    public Task<byte[]?> GetImageProductByIdAsync(Guid id);
    public IQueryable<Product?> GetProductsByCategory(Guid id);
    public Task AddImageToProductByIdAsync(Guid id, byte[] image);
}