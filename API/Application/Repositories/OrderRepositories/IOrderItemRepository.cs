﻿using Domain.Entities.Orders;

namespace Application.Repositories.OrderRepositories;

public interface IOrderItemRepository : IBaseRepository<OrderItem>
{
    
}