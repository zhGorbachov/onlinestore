﻿using Domain.Entities.Orders;

namespace Application.Repositories.OrderRepositories;

public interface IOrderRepository : IBaseRepository<Order>
{
    public Task AddOrderItemToOrderAsync(OrderItem orderItem, Guid id);
    public Task<ICollection<OrderItem>?> GetAllOrderItemsFromOrderAsync(Guid id);
}