﻿using Domain.Common;

namespace Application.Repositories;

public interface IBaseRepository<T> where T : BaseEntity
{
    public Task AddAsync(T entity);
    public Task UpdateAsync(T entity, Guid id);
    public Task DeleteAsync(Guid id);
    public Task<T> GetByIdAsync(Guid id);
    public IQueryable<T> GetAll();
}