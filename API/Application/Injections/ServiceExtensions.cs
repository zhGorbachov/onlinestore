﻿using System.Reflection;
using Application.Features.Interfaces;
using Application.Features.Interfaces.OrderServiceInterfaces;
using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Features.Services;
using Application.Features.Services.OrderServices;
using Application.Features.Services.ProductServices;
using Application.Features.Services.UserServices;
using Application.Mapper;
using Microsoft.Extensions.DependencyInjection;

namespace Application.Injections;

public static class ServiceExtensions
{
    public static void ConfigureApplication(this IServiceCollection services)
    {
        services.AddAutoMapper(Assembly.GetExecutingAssembly());
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IAddressService, AddressService>();
        services.AddScoped<IPaymentMethodService, PaymentMethodService>();
        services.AddScoped<IProductService, ProductService>();
        services.AddScoped<ICategoryService, CategoryService>();
        services.AddScoped<IRatingService, RatingService>();
        services.AddScoped<IReviewService, ReviewService>();
        services.AddScoped<IOrderService, OrderService>();
        services.AddScoped<IOrderItemService, OrderItemService>();

        services.AddScoped<IJwtService, JwtService>();
        services.AddScoped<IAuthorizeService, AuthorizeService>();
    }
}