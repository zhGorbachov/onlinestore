﻿namespace Application.Models.OrderModels;

public class OrderItemModel : BaseModel
{
    public int Quantity { get; set; }
    public decimal Subtotal { get; set; }
    public Guid OrderId { get; set; }
    public Guid ProductId { get; set; }
}