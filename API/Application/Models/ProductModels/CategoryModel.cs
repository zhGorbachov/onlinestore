﻿namespace Application.Models.ProductModels;

public class CategoryModel : BaseModel
{
    public string Name { get; set; }
    public string? Description { get; set; }
    public Guid? ParentCategoryId { get; set; }
}