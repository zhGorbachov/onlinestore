﻿namespace Application.Models.ProductModels;

public class RatingModel : BaseModel
{
    public int Grade { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
}