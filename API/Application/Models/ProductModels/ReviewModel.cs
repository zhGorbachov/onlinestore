﻿namespace Application.Models.ProductModels;

public class ReviewModel : BaseModel
{
    public string Text { get; set; }
    public Guid UserId { get; set; }
    public Guid ProductId { get; set; }
}