﻿namespace Application.Models.UserModels;

public class PaymentMethodModel : BaseModel
{
    public string CardNumber { get; set; }
    public string ExpireDate { get; set; }
    public Guid UserId { get; set; }
}