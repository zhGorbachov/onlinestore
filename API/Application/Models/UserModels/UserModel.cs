﻿namespace Application.Models.UserModels;

public class UserModel : BaseModel
{
    public string FirstName { get; set; }
    public string SecondName { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
}