﻿namespace Application.Models.UserModels;

public class AddressModel : BaseModel
{
    public string StreetAddress { get; set; }
    public string City { get; set; }
    public string ZipCode { get; set; }
    public Guid UserId { get; set; }
}