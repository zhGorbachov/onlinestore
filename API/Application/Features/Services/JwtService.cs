﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Application.Features.Interfaces;
using Application.Models.UserModels;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Application.Features.Services;

public class JwtService : IJwtService
{
    private readonly IConfiguration _configuration;

    public JwtService(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public string CreateJwt(UserModel userModel)
    {
        var secretKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_configuration["Key"] ?? string.Empty));
        var credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

        var claims = new List<Claim>
        {
            new Claim(ClaimTypes.Name, userModel.FirstName),
            new Claim(JwtRegisteredClaimNames.Sub, userModel.FirstName),
            new Claim(JwtRegisteredClaimNames.Sub, userModel.SecondName),
            new Claim("Id", userModel.Id.ToString()),
            new Claim(JwtRegisteredClaimNames.UniqueName, userModel.Username),
            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
        };

        var token = new JwtSecurityToken
        (
            issuer: _configuration["ISSUER"],
            audience: _configuration["AUDIENCE"],
            claims: claims,
            expires: DateTime.Now.AddHours(2),
            signingCredentials: credentials
        );
        
        return new JwtSecurityTokenHandler().WriteToken(token);

    }
}