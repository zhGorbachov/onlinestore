﻿using Application.Features.Interfaces.OrderServiceInterfaces;
using Application.Models.OrderModels;
using Application.Repositories.OrderRepositories;
using AutoMapper;
using Domain.Entities.Orders;

namespace Application.Features.Services.OrderServices;

public class OrderService : IOrderService
{
    private readonly IOrderRepository _orderRepository;
    private readonly IMapper _mapper;

    public OrderService(IOrderRepository orderRepository, IMapper mapper)
    {
        _orderRepository = orderRepository;
        _mapper = mapper;
    }
    
    public async Task AddOrderAsync(OrderModel orderModel)
    {
        var order = _mapper.Map<Order>(orderModel);
        await _orderRepository.AddAsync(order);
    }

    public async Task UpdateOrderAsync(OrderModel orderModel, Guid id)
    {
        var order = _mapper.Map<Order>(orderModel);
        await _orderRepository.UpdateAsync(order, id);
    }

    public async Task DeleteOrderAsync(Guid id)
    {
        await _orderRepository.DeleteAsync(id);
    }

    public async Task<OrderModel> GetOrderByIdAsync(Guid id)
    {
        var order = await _orderRepository.GetByIdAsync(id);
        return _mapper.Map<OrderModel>(order);
    }

    public IQueryable<OrderModel> GetAllOrderModels()
    {
        var orders = _orderRepository.GetAll();
        return _mapper.ProjectTo<OrderModel>(orders);
    }

    public async Task AddOrderItemModelToOrderAsync(OrderItemModel orderItemModel, Guid id)
    {
        var orderItem = _mapper.Map<OrderItem>(orderItemModel);
        await _orderRepository.AddOrderItemToOrderAsync(orderItem, id);
    }

    public async Task<IQueryable<OrderItemModel>?> GetAllOrderItemModelsFromOrderAsync(Guid id)
    {
        var orderItems = await _orderRepository.GetAllOrderItemsFromOrderAsync(id);
        return _mapper.ProjectTo<OrderItemModel>(orderItems?.AsQueryable());
    }
}