﻿using Application.Features.Interfaces.OrderServiceInterfaces;
using Application.Models.OrderModels;
using Application.Repositories.OrderRepositories;
using AutoMapper;
using Domain.Entities.Orders;

namespace Application.Features.Services.OrderServices;

public class OrderItemService : IOrderItemService
{
    private readonly IOrderItemRepository _orderItemRepository;
    private readonly IMapper _mapper;

    public OrderItemService(IOrderItemRepository orderItemRepository, IMapper mapper)
    {
        _orderItemRepository = orderItemRepository;
        _mapper = mapper;
    }
    
    public async Task AddOrderItemAsync(OrderItemModel orderItemModel)
    {
        var orderItem = _mapper.Map<OrderItem>(orderItemModel);
        await _orderItemRepository.AddAsync(orderItem);
    }

    public async Task UpdateOrderItemAsync(OrderItemModel orderItemModel, Guid id)
    {
        var orderItem = _mapper.Map<OrderItem>(orderItemModel);
        await _orderItemRepository.UpdateAsync(orderItem, id);
    }

    public async Task DeleteOrderItemAsync(Guid id)
    {
        await _orderItemRepository.DeleteAsync(id);
    }

    public async Task<OrderItemModel> GetOrderItemByIdAsync(Guid id)
    {
        var orderItem = await _orderItemRepository.GetByIdAsync(id);
        return _mapper.Map<OrderItemModel>(orderItem);
    }

    public IQueryable<OrderItemModel> GetAllOrderItemModels()
    {
        var orderItems = _orderItemRepository.GetAll();
        return _mapper.ProjectTo<OrderItemModel>(orderItems);
    }
}