﻿using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using Application.Repositories.UserRepositories;
using AutoMapper;
using Domain.Entities.Users;

namespace Application.Features.Services.UserServices;

public class PaymentMethodService : IPaymentMethodService
{
    private readonly IPaymentMethodRepository _paymentMethodRepository;
    private readonly IMapper _mapper;

    public PaymentMethodService(IPaymentMethodRepository paymentMethodRepository, IMapper mapper)
    {
        _paymentMethodRepository = paymentMethodRepository;
        _mapper = mapper;
    }
    
    public async Task AddPaymentMethodAsync(PaymentMethodModel paymentMethodModel)
    {
        var paymentMethod = _mapper.Map<PaymentMethod>(paymentMethodModel);
        await _paymentMethodRepository.AddAsync(paymentMethod);
    }

    public async Task UpdatePaymentMethodAsync(PaymentMethodModel paymentMethodModel, Guid id)
    {
        var paymentMethod = _mapper.Map<PaymentMethod>(paymentMethodModel);
        await _paymentMethodRepository.UpdateAsync(paymentMethod, id);
    }

    public async Task DeletePaymentMethodAsync(Guid id)
    {
        await _paymentMethodRepository.DeleteAsync(id);
    }

    public async Task<PaymentMethodModel> GetPaymentMethodByIdAsync(Guid id)
    {
        var paymentMethod = await _paymentMethodRepository.GetByIdAsync(id);
        return _mapper.Map<PaymentMethodModel>(paymentMethod);
    }

    public IQueryable<PaymentMethodModel> GetAllPaymentMethodModels()
    {
        var paymentMethods = _paymentMethodRepository.GetAll();
        return _mapper.ProjectTo<PaymentMethodModel>(paymentMethods);
    }
}