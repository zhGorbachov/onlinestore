﻿using Application.Features.Interfaces;
using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using Application.Repositories.UserRepositories;
using AutoMapper;
using Domain.Entities.Users;

namespace Application.Features.Services.UserServices;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;
    private readonly IMapper _mapper;

    public UserService(IUserRepository userRepository, IMapper mapper)
    {
        _userRepository = userRepository;
        _mapper = mapper;
    }
    
    public async Task AddUserAsync(UserModel userModel)
    {
        var user = _mapper.Map<User>(userModel);
        await _userRepository.AddAsync(user);
    }

    public async Task UpdateUserAsync(UserModel userModel, Guid id)
    {
        var user = _mapper.Map<User>(userModel);
        await _userRepository.UpdateAsync(user, id);
    }

    public async Task DeleteUserAsync(Guid id)
    {
        await _userRepository.DeleteAsync(id);
    }

    public async Task<UserModel> GetUserByIdAsync(Guid id)
    {
        var user = await _userRepository.GetByIdAsync(id);
        return _mapper.Map<UserModel>(user);
    }

    public IQueryable<UserModel> GetAllUserModels()
    {
        var users = _userRepository.GetAll();
        return _mapper.ProjectTo<UserModel>(users);
    }

    public async Task<UserModel> GetUserModeByUsernameAsync(string username)
    {
        var user = await _userRepository.GetByUsernameAsync(username);
        return _mapper.Map<UserModel>(user);
    }

    public async Task<UserModel> GetUserModelByEmailAsync(string email)
    {
        var user = await _userRepository.GetByEmailAsync(email);
        return _mapper.Map<UserModel>(user);
    }
}