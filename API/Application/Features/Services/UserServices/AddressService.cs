﻿using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using Application.Repositories.UserRepositories;
using AutoMapper;
using Domain.Entities.Users;

namespace Application.Features.Services.UserServices;

public class AddressService : IAddressService
{
    private readonly IAddressRepository _addressRepository;
    private readonly IMapper _mapper;

    public AddressService(IAddressRepository addressRepository, IMapper mapper)
    {
        _addressRepository = addressRepository;
        _mapper = mapper;
    }
    
    public async Task AddAddressAsync(AddressModel addressModel)
    {
        var address = _mapper.Map<Address>(addressModel);
        await _addressRepository.AddAsync(address);
    }

    public async Task UpdateAddressAsync(AddressModel addressModel, Guid id)
    {
        var address = _mapper.Map<Address>(addressModel);
        await _addressRepository.UpdateAsync(address, id);
    }

    public async Task DeleteAddressAsync(Guid id)
    {
        await _addressRepository.DeleteAsync(id);
    }

    public async Task<AddressModel> GetAddressByIdAsync(Guid id)
    {
        var address = await _addressRepository.GetByIdAsync(id);
        return _mapper.Map<AddressModel>(address);
    }

    public IQueryable<AddressModel> GetAllAddressModels()
    {
        var addresses = _addressRepository.GetAll();
        return _mapper.ProjectTo<AddressModel>(addresses);
    }
}