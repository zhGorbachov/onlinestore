﻿using Application.Features.Interfaces;
using Application.Features.Interfaces.UserServiceInterfaces;
using Application.Models.UserModels;
using AutoMapper;
using Microsoft.Extensions.Configuration;

namespace Application.Features.Services;

public class AuthorizeService : IAuthorizeService
{
    private readonly IUserService _userService;
    private readonly IConfiguration _configuration;
    private readonly IJwtService _jwtService;

    public AuthorizeService(IUserService userService, IConfiguration configuration, IJwtService jwtService)
    {
        _userService = userService;
        _configuration = configuration;
        _jwtService = jwtService;
    }

    public async Task RegisterUserAsync(UserModel userModel)
    {
        if (await CheckUserUsername(userModel.Username) && await CheckUserEmailAsync(userModel.Email))
            await _userService.AddUserAsync(userModel);
    }

    public string LoginUser(UserModel userModel)
    {
        var users = _userService.GetAllUserModels();
        var user = users.Any(x => x.Username == userModel.Username && x.Password == userModel.Password);

        if (user == false) return "";
        var token = _jwtService.CreateJwt(userModel);

        return token;

    }

    private async Task<bool> CheckUserEmailAsync(string email)
    {
        var user = await _userService.GetUserModelByEmailAsync(email);

        return user != null;
    }

    private async Task<bool> CheckUserUsername(string username)
    {
        var user = await _userService.GetUserModeByUsernameAsync(username);

        return user != null;
    }
}