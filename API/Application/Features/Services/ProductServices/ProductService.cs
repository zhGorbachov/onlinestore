﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using Application.Repositories.ProductRepositories;
using AutoMapper;
using Domain.Entities.Products;

namespace Application.Features.Services.ProductServices;

public class ProductService : IProductService
{
    private readonly IProductRepository _productRepository;
    private readonly IMapper _mapper;

    public ProductService(IProductRepository productRepository, IMapper mapper)
    {
        _productRepository = productRepository;
        _mapper = mapper;
    }
    
    public async Task AddProductAsync(ProductModel productModel)
    {
        var product = _mapper.Map<Product>(productModel);
        await _productRepository.AddAsync(product);
    }

    public async Task UpdateProductAsync(ProductModel productModel, Guid id)
    {
        var product = _mapper.Map<Product>(productModel);
        await _productRepository.UpdateAsync(product, id);
    }

    public async Task DeleteProductAsync(Guid id)
    {
        await _productRepository.DeleteAsync(id);
    }

    public async Task<ProductModel> GetProductByIdAsync(Guid id)
    {
        var product = await _productRepository.GetByIdAsync(id);
        return _mapper.Map<ProductModel>(product);
    }

    public IQueryable<ProductModel> GetAllProductModels()
    {
        var products = _productRepository.GetAll();
        return _mapper.ProjectTo<ProductModel>(products);
    }

    public async Task<int?> GetQuantityOfProductAsync(Guid id)
    {
        return await _productRepository.GetQuantityOfProductAsync(id);
    }

    public async Task<byte[]?> GetImageProductByIdAsync(Guid id)
    {
        return await _productRepository.GetImageProductByIdAsync(id);
    }

    public IQueryable<ProductModel?> GetProductModelsByCategory(Guid id)
    {
        var products = _productRepository.GetProductsByCategory(id);
        return _mapper.ProjectTo<ProductModel>(products);
    }

    public async Task AddImageToProductModelByIdAsync(Guid id, byte[] image)
    {
        await _productRepository.AddImageToProductByIdAsync(id, image);
    }
}