﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using Application.Repositories.ProductRepositories;
using AutoMapper;
using Domain.Entities.Products;

namespace Application.Features.Services.ProductServices;

public class ReviewService : IReviewService
{
    private readonly IReviewRepository _reviewRepository;
    private readonly IMapper _mapper;

    public ReviewService(IReviewRepository reviewRepository, IMapper mapper)
    {
        _reviewRepository = reviewRepository;
        _mapper = mapper;
    }
    
    public async Task AddReviewAsync(ReviewModel reviewModel)
    {
        var review = _mapper.Map<Review>(reviewModel);
        await _reviewRepository.AddAsync(review);
    }

    public async Task UpdateReviewAsync(ReviewModel reviewModel, Guid id)
    {
        var review = _mapper.Map<Review>(reviewModel);
        await _reviewRepository.UpdateAsync(review, id);
    }

    public async Task DeleteReviewAsync(Guid id)
    {
        await _reviewRepository.DeleteAsync(id);
    }

    public async Task<ReviewModel> GetReviewByIdAsync(Guid id)
    {
        var review = await _reviewRepository.GetByIdAsync(id);
        return _mapper.Map<ReviewModel>(review);
    }

    public IQueryable<ReviewModel> GetAllReviewModels()
    {
        var reviews = _reviewRepository.GetAll();
        return _mapper.ProjectTo<ReviewModel>(reviews);
    }
}