﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using Application.Repositories.ProductRepositories;
using AutoMapper;
using Domain.Entities.Products;

namespace Application.Features.Services.ProductServices;

public class CategoryService : ICategoryService
{
    private readonly ICategoryRepository _categoryRepository;
    private readonly IMapper _mapper;

    public CategoryService(ICategoryRepository categoryRepository, IMapper mapper)
    {
        _categoryRepository = categoryRepository;
        _mapper = mapper;
    }
    
    public async Task AddCategoryAsync(CategoryModel categoryModel)
    {
        var category = _mapper.Map<Category>(categoryModel);
        await _categoryRepository.AddAsync(category);
    }

    public async Task UpdateCategoryAsync(CategoryModel categoryModel, Guid id)
    {
        var category = _mapper.Map<Category>(categoryModel);
        await _categoryRepository.UpdateAsync(category, id);
    }

    public async Task DeleteCategoryAsync(Guid id)
    {
        await _categoryRepository.DeleteAsync(id);
    }

    public async Task<CategoryModel> GetCategoryByIdAsync(Guid id)
    {
        var category = await _categoryRepository.GetByIdAsync(id);
        return _mapper.Map<CategoryModel>(category);
    }

    public IQueryable<CategoryModel> GetAllCategoryModels()
    {
        var categories = _categoryRepository.GetAll();
        return _mapper.ProjectTo<CategoryModel>(categories);
    }

    public async Task<IQueryable<CategoryModel>?> GetSubCategoryModelsAsync(Guid id)
    {
        var categories = await _categoryRepository.GetSubCategoriesAsync(id);

        return _mapper.ProjectTo<CategoryModel>(categories);
    }
}