﻿using Application.Features.Interfaces.ProductServiceInterfaces;
using Application.Models.ProductModels;
using Application.Repositories.ProductRepositories;
using AutoMapper;
using Domain.Entities.Products;

namespace Application.Features.Services.ProductServices;

public class RatingService : IRatingService
{
    private readonly IRatingRepository _ratingRepository;
    private readonly IMapper _mapper;

    public RatingService(IRatingRepository ratingRepository, IMapper mapper)
    {
        _ratingRepository = ratingRepository;
        _mapper = mapper;
    }
    
    public async Task AddRatingAsync(RatingModel ratingModel)
    {
        var rating = _mapper.Map<Rating>(ratingModel);
        await _ratingRepository.AddAsync(rating);
    }

    public async Task UpdateRatingAsync(RatingModel ratingModel, Guid id)
    {
        var rating = _mapper.Map<Rating>(ratingModel);
        await _ratingRepository.UpdateAsync(rating, id);
    }

    public async Task DeleteRatingAsync(Guid id)
    {
        await _ratingRepository.DeleteAsync(id);
    }

    public async Task<RatingModel> GetRatingByIdAsync(Guid id)
    {
        var rating = await _ratingRepository.GetByIdAsync(id);
        return _mapper.Map<RatingModel>(rating);
    }

    public IQueryable<RatingModel> GetAllRatingModels()
    {
        var ratings = _ratingRepository.GetAll();
        return _mapper.ProjectTo<RatingModel>(ratings);
    }
}