﻿using Application.Models.ProductModels;

namespace Application.Features.Interfaces.ProductServiceInterfaces;

public interface IProductService
{
    public Task AddProductAsync(ProductModel productModel);
    public Task UpdateProductAsync(ProductModel productModel, Guid id);
    public Task DeleteProductAsync(Guid id);
    public Task<ProductModel> GetProductByIdAsync(Guid id);
    public IQueryable<ProductModel> GetAllProductModels();
    public Task<int?> GetQuantityOfProductAsync(Guid id);
    public Task<byte[]?> GetImageProductByIdAsync(Guid id);
    public IQueryable<ProductModel?> GetProductModelsByCategory(Guid id);
    public Task AddImageToProductModelByIdAsync(Guid id, byte[] image);
}