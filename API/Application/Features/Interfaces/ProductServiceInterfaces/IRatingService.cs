﻿using Application.Models.ProductModels;

namespace Application.Features.Interfaces.ProductServiceInterfaces;

public interface IRatingService
{
    public Task AddRatingAsync(RatingModel ratingModel);
    public Task UpdateRatingAsync(RatingModel ratingModel, Guid id);
    public Task DeleteRatingAsync(Guid id);
    public Task<RatingModel> GetRatingByIdAsync(Guid id);
    public IQueryable<RatingModel> GetAllRatingModels();
}