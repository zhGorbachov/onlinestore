﻿using Application.Models.ProductModels;

namespace Application.Features.Interfaces.ProductServiceInterfaces;

public interface ICategoryService
{
    public Task AddCategoryAsync(CategoryModel categoryModel);
    public Task UpdateCategoryAsync(CategoryModel categoryModel, Guid id);
    public Task DeleteCategoryAsync(Guid id);
    public Task<CategoryModel> GetCategoryByIdAsync(Guid id);
    public IQueryable<CategoryModel> GetAllCategoryModels();
    public Task<IQueryable<CategoryModel>?> GetSubCategoryModelsAsync(Guid id);
}