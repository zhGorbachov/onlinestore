﻿using Application.Models.ProductModels;

namespace Application.Features.Interfaces.ProductServiceInterfaces;

public interface IReviewService
{
    public Task AddReviewAsync(ReviewModel reviewModel);
    public Task UpdateReviewAsync(ReviewModel reviewModel, Guid id);
    public Task DeleteReviewAsync(Guid id);
    public Task<ReviewModel> GetReviewByIdAsync(Guid id);
    public IQueryable<ReviewModel> GetAllReviewModels();
}