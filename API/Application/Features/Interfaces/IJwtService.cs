﻿using Application.Models.UserModels;
using Microsoft.Extensions.Configuration;

namespace Application.Features.Interfaces;

public interface IJwtService
{
    public string CreateJwt(UserModel userModel);
}