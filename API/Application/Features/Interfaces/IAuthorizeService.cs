﻿using Application.Models.UserModels;

namespace Application.Features.Interfaces;

public interface IAuthorizeService
{
    public Task RegisterUserAsync(UserModel userModel);
    public string LoginUser(UserModel userModel);
}