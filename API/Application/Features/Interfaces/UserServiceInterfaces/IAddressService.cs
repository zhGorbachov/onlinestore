﻿using Application.Models.UserModels;

namespace Application.Features.Interfaces.UserServiceInterfaces;

public interface IAddressService
{
    public Task AddAddressAsync(AddressModel addressModel);
    public Task UpdateAddressAsync(AddressModel addressModel, Guid id);
    public Task DeleteAddressAsync(Guid id);
    public Task<AddressModel> GetAddressByIdAsync(Guid id);
    public IQueryable<AddressModel> GetAllAddressModels();
}