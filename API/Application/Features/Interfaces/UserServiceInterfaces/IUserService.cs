﻿using Application.Models.UserModels;

namespace Application.Features.Interfaces.UserServiceInterfaces;

public interface IUserService
{
    public Task AddUserAsync(UserModel userModel);
    public Task UpdateUserAsync(UserModel userModel, Guid id);
    public Task DeleteUserAsync(Guid id);
    public Task<UserModel> GetUserByIdAsync(Guid id);
    public IQueryable<UserModel> GetAllUserModels();
    public Task<UserModel> GetUserModeByUsernameAsync(string username);
    public Task<UserModel> GetUserModelByEmailAsync(string email);
}