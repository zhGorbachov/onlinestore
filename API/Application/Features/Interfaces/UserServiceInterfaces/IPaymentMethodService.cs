﻿using Application.Models.UserModels;

namespace Application.Features.Interfaces.UserServiceInterfaces;

public interface IPaymentMethodService
{
    public Task AddPaymentMethodAsync(PaymentMethodModel paymentMethodModel);
    public Task UpdatePaymentMethodAsync(PaymentMethodModel paymentMethodModel, Guid id);
    public Task DeletePaymentMethodAsync(Guid id);
    public Task<PaymentMethodModel> GetPaymentMethodByIdAsync(Guid id);
    public IQueryable<PaymentMethodModel> GetAllPaymentMethodModels();
}