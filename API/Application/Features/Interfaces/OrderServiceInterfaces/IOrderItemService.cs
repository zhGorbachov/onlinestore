﻿using Application.Models.OrderModels;

namespace Application.Features.Interfaces.OrderServiceInterfaces;

public interface IOrderItemService
{
    public Task AddOrderItemAsync(OrderItemModel orderItemModel);
    public Task UpdateOrderItemAsync(OrderItemModel orderItemModel, Guid id);
    public Task DeleteOrderItemAsync(Guid id);
    public Task<OrderItemModel> GetOrderItemByIdAsync(Guid id);
    public IQueryable<OrderItemModel> GetAllOrderItemModels();
}