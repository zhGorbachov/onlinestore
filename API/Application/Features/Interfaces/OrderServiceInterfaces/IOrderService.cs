﻿using Application.Models.OrderModels;

namespace Application.Features.Interfaces.OrderServiceInterfaces;

public interface IOrderService
{
    public Task AddOrderAsync(OrderModel orderModel);
    public Task UpdateOrderAsync(OrderModel orderModel, Guid id);
    public Task DeleteOrderAsync(Guid id);
    public Task<OrderModel> GetOrderByIdAsync(Guid id);
    public IQueryable<OrderModel> GetAllOrderModels();
    public Task AddOrderItemModelToOrderAsync(OrderItemModel orderItemModel, Guid id);
    public Task<IQueryable<OrderItemModel>?> GetAllOrderItemModelsFromOrderAsync(Guid id);
}