﻿using Application.Models.OrderModels;
using Application.Models.ProductModels;
using Application.Models.UserModels;
using AutoMapper;
using Domain.Entities.Orders;
using Domain.Entities.Products;
using Domain.Entities.Users;

namespace Application.Mapper;

public class MapperProfile : Profile
{
    public MapperProfile()
    {
        CreateMap<User, UserModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ForMember(x => x.Username,
                opt => opt.MapFrom(model => model.Username))
            .ForMember(x => x.Password,
                opt => opt.MapFrom(model => model.Password))
            .ForMember(x => x.Email,
                opt => opt.MapFrom(model => model.Email))
            .ReverseMap();
        
        CreateMap<Address, AddressModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<PaymentMethod, PaymentMethodModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<Order, OrderModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<OrderItem, OrderItemModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<Product, ProductModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<Category, CategoryModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<Rating, RatingModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
        
        CreateMap<Review, ReviewModel>()
            .ForMember(x => x.Id,
                opt => opt.MapFrom(model => model.Id))
            .ReverseMap();
    }
}